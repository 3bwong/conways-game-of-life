'use strict';

const express = require('express');
const path = require('path');
const _ = require('lodash');
const wss = require('./wsserver');

const PORT = process.env.PORT || 4000;
const INDEX = path.join(__dirname, 'browser/index.html');
const GAME = path.join(__dirname, 'browser/game.html');
const app = express();

app.get('/', (req, res) => res.status(200).sendFile(INDEX));
app.get('/game', function (req, res) {
    res.status(200).sendFile(GAME);
});
app.get('/assets/*', function (req, res) {
    res.sendFile(__dirname + '/browser/' + req.path);
});

const server = app.listen(PORT, () => console.log(`Listening on ${PORT}`));

wss(server);

module.exports = server;
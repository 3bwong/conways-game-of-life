
const SocketServer = require('ws').Server;
const gameAction = require('./game-action');

function connectScoket(server) {
    const wss = new SocketServer({ server });
    wss.on('connection', (ws, req) => {
        console.log('Client connected');
        ws.on("message", (message, req) => {
            try {
                let data = JSON.parse(message);
                if (!data.action) {
                    ws.send("Message Error.");
                    ws.close();
                } else {
                    switch (data.action) {
                        case "new":
                            gameAction.newPlayer(ws);
                            break;
                        case "update":
                            gameAction.updateTable(data);
                            break;
                        case "restore":
                            gameAction.restoreGame(ws, data);
                            break;
                        case "quit":
                            gameAction.quitGame(ws, data);
                            break;
                        default:
                            ws.send("Action Undefined");
                            ws.close();
                            break;
                    }
                }
            } catch (err) {
                console.log(err)
                ws.send("Message Error");
                ws.close();
            }
        })
        ws.on('close', () => {
            console.log("Client Disconnected")
            setTimeout(() => {
                gameAction.removePlayer(ws);
            }, 18000)
        });
    });

    setInterval(() => {
        gameAction.nextGeneration();
    }, 3000);
}

module.exports = connectScoket;
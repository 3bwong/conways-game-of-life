jQuery(document).ready(function ($) {
    const drawer = mdc.drawer.MDCDrawer.attachTo(document.querySelector('.mdc-drawer'));
    // WebSocket Connection
    var HOST = location.origin.replace(/^http/, 'ws')
    var ws = new WebSocket(HOST);
    var player, list;

    // Draw 20x20 Table
    $('#conways-game-content').ready(function () {
        let tableHTML = "<table>";
        for (let x = 0; x < 20; x++) {
            tableHTML += '<tr>';
            for (let y = 0; y < 20; y++) {
                tableHTML += `<td id="cell-${x}-${y}"></td>`
            }
            tableHTML += '</tr>'
        }
        tableHTML += "</table>"
        $('#conways-game-table').html(tableHTML);
    })

    // Table Cell Click
    $(document).on('click', '#conways-game-table td', function () {
        if (player) {
            var reqObj = {
                action: "update",
                roomId: player.roomId,
                color: player.color,
                type: "cell",
                location: $(this).attr('id')
            }
            ws.send(JSON.stringify(reqObj))
        }
    })

    // Pattern Sidebar Click
    $(document).on('click', '.mdc-list-item', function () {
        $('.mdc-drawer').addClass('mdc-drawer--closing');
        drawer.open = false;
        $('#conways-pattern-btn').focus();
        if (player) {
            var reqObj = {
                action: "update",
                roomId: player.roomId,
                color: player.color,
                type: "pattern",
                pattern: $(this).text().trim()
            }
            ws.send(JSON.stringify(reqObj));
        }

    })

    // MDC Drawer Control
    $('#conways-pattern-btn').click(function () {
        drawer.open = true;
    })
    $(document).on('click', '.mdc-drawer-scrim', function () {
        if (drawer.open) {
            $('.mdc-drawer').addClass('mdc-drawer--closing');
            drawer.open = false;
        }
    })

    // Quit Game
    $('#conways-quit-btn').click(function () {
        if (player) {
            let reqObj = Object.assign({}, player);
            reqObj.action = "quit";
            ws.send(JSON.stringify(reqObj));
        }
    })

    // WebSocket Events
    ws.onopen = function (event) {
        var currentRoom = sessionStorage.getItem('conways_game_room');
        if (currentRoom) {
            player = JSON.parse(currentRoom);
            let reqObj = Object.assign({}, player);
            reqObj.action = "restore";
            $('#conways-game .conways-title').text("Room #" + (player.roomIndex + 1));
            ws.send(JSON.stringify(reqObj));
        } else {
            ws.send(JSON.stringify({ action: "new" }));
        }
    };
    ws.onerror = function (event) {
        alert(`[error] ${error.message}. Please refresh the page.`);
    };
    ws.onmessage = function (event) {
        try {
            let data = JSON.parse(event.data);
            if (data) {
                switch (data.action) {
                    case "new":
                        player = {
                            roomId: data.roomId,
                            color: data.color,
                            roomIndex: data.roomIndex
                        }
                        $('#conways-game .conways-title').text("Room #" + (player.roomIndex + 1));
                        updatePatterns(data.patterns);
                        sessionStorage.setItem('conways_game_room', JSON.stringify(player));
                        break;
                    case "update":
                        updateCurrentPlayers(data.room.players, player.color);
                        updateTable(data.room.table)
                        break;
                    case "quit":
                        sessionStorage.removeItem('conways_game_room');
                        location.replace('/');
                        break;
                }
            }
        } catch (err) {
            console.log(err);
        }
    };
    ws.onclose = function (event) {
        player = null;
        setTimeout(function () {
            sessionStorage.removeItem('conways_game_room');
            location.replace('/');
        }, 18000);
    }

    function updateCurrentPlayers(players, color) {
        $('#conways-game-players').html('');
        for (let i = 0; i < players.length; i++) {
            if (players[i] != color) {
                let html = `
                <div class="conways-current-players">
                    <div class="player-color" style="background:rgb(${players[i]})"></div>
                    <span> Player #${i + 1} </span>
                </div>
                `;
                $('#conways-game-players').append(html);
            }
        }
        $('#conways-game-players').append(`
            <div class="conways-current-players">
                <div class="player-color" style="background:rgb(${color})"></div>
                <span> You </span>
            </div>
            `)
    }

    function updatePatterns(patterns) {
        $('#conways-game-pattern .mdc-drawer__content').html('');
        for (let pattern of patterns) {
            $('#conways-game-pattern .mdc-drawer__content').append(`
            <h3 class="mdc-list-group__subheader">${pattern.type}</h3>
            `);
            var html = '<ul class="mdc-list">';
            for (let life of pattern.lifes) {
                html += `
                    <li class="mdc-list-item">
                        <span class="mdc-list-item__text">${life.name}</span>
                    </li>
                `;
            }
            html += "</ul>";
            $('#conways-game-pattern .mdc-drawer__content').append(html);
        }
    }

    function updateTable(table) {
        if (table) {
            for (let x = 0; x < table.length; x++) {
                for (let y = 0; y < table.length; y++) {
                    if (table[x][y] !== "#fff") {
                        $(`#cell-${x}-${y}`).css("background", `rgb(${table[x][y]})`);
                    } else {
                        $(`#cell-${x}-${y}`).css("background", "#fff");
                    }
                }
            }
        }
    }
})
# Conway's Game of Life
This is a multiplayer Web App version of Game of Life. The web app will create a room consist of at most 10 players to play with, so that there will not be too many players in a single board. Each player will be assigned with a color and room ID which are saved in the session storage (to recognise one from lost of connection) in the begining. The generation of cells will step every 3 seconds to give time for players to add cells on the board. The detailed rules of game can be found on [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life).
## Getting Started
These instructions will guide you through the installation on your local machine for development and testing purposes.
### Prerequisities
This Web App is based in Node.js. Please intall Node.js and NPM before start. See [installation guide](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
### Protocol support
* HyBi drafts 07-12 (Use the option protocolVersion: 8)
* HyBi drafts 13-17 (Current default, alternatively option protocolVersion: 13)
See more supporting details on [WebSocket](https://github.com/websockets/ws)
### Installation
Download the current project and install the modules in the project root:
```
$ npm install
```
To start the project:
```
$ npm start
```
## Testing
```
$ npm test
```
## Built With
* [Express](https://github.com/expressjs/express) - Proxy Server
* [WebSocket](https://github.com/websockets/ws) - WebSocket Server
* [Lodash](https://lodash.com/) - Dependency
## License
This project is licensed under the ISC License - see the [LICENSE](/LICENSE) file for details
## Acknowledgments
* Heroku Examples - [node-websockets](https://github.com/heroku-examples/node-websockets)

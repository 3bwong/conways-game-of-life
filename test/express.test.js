var request = require('supertest');
describe('loading express', function () {
  var server;
  beforeEach(function () {
    server = require('../server');
  });
  afterEach(function () {
    server.close();
  });
  it('path: /', function testSlash(done) {
  request(server)
    .get('/')
    .expect(200, done);
  });
  it('path: /game', function testPath(done) {
    request(server)
      .get('/game')
      .expect(200, done);
  });
  it('path: /assets/scripts/game.js', function testPath(done) {
    request(server)
      .get('/assets/scripts/game.js')
      .expect(200, done);
  });
  it('path: /assets/styles/game.css', function testPath(done) {
    request(server)
      .get('/assets/styles/game.css')
      .expect(200, done);
  });
  it('path: some other (404)', function testPath(done) {
    request(server)
      .get('/test/path')
      .expect(404, done);
  });
});
var assert = require('assert');
const WebSocket = require('ws');
const PORT = process.env.PORT || 4001;
const wss = require('../wsserver');
const express = require('express');
const app = express();

describe('WebSocket Testing', function () {
    const server = app.listen(PORT, () => console.log(`Listening on ${PORT}`));
    wss(server);
    var ws, player;

    beforeEach(function (done) {
        ws = new WebSocket('http://localhost:' + PORT);
        ws.on('open', function () {
            console.log('WebSocket connected.');
            done();
        });
        ws.on('error', (data) => {
            console.log(data);
        })
        ws.on('close', function () {
            console.log('WebSocket closed.');
        })
    });

    afterEach(function (done) {
        if (ws.readyState == 1) {
            console.log('disconnecting...');
            ws.close();
        } else {
            console.log('WebSocket is closed already.');
        }
        done();
    });

    describe('Request action test', function () {
        it('Test: action = new', function (done) {
            ws.send(JSON.stringify({ action: "new" }));
            ws.on('message', (data) => {
                try {
                    if (JSON.parse(data).action === "new") {
                        player = {
                            roomId: data.roomId,
                            color: data.color
                        }
                        assert.ok(true);
                        console.log(`Action: Created new player`);
                    }
                    if (data && JSON.parse(data).action === "update") {
                        assert.ok(true);
                        console.log(`Action: Sent updated room info`);
                    }
                } catch (e) {
                    assert.fail();
                    console.log(`Error: ${e}`);
                }
            })
            done();
        });

        it('Test: action = restore', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: "restore" }, player)));
            ws.on('message', (data) => {
                try {
                    if (JSON.parse(data).action === "new") {
                        player = {
                            roomId: data.roomId,
                            color: data.color
                        }
                        assert.ok(true);
                        console.log(`Action: Room / Player not found. Created new player.`);
                    }
                    if (JSON.parse(data).action === "update") {
                        assert.ok(true);
                        console.log(`Action: Room and Player found. Sent updated room info with new table`);
                    }
                } catch (e) {
                    assert.fail();
                    console.log(`Error: ${e}`);
                }
            })
            done();
        });

        it('Test: action = update (cell)', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: "update", type: "cell", location: "cell-0-0" }, player)));
            ws.on('message', (data) => {
                try {
                    if (JSON.parse(data).action === "update") {
                        assert.ok(true);
                        console.log(`Action: Sent updated room info with new table`);
                    }
                } catch (e) {
                    assert.fail();
                    console.log(`Error: ${e}`);
                }
            })
            done();
        });

        it('Test: action = update (pattern)', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: "update", type: "pattern", pattern: "Block" }, player)));
            ws.on('message', (data) => {
                try {
                    if (JSON.parse(data).action === "update") {
                        assert.ok(true);
                        console.log(`Action: Sent updated room info with new table`);
                    }
                } catch (e) {
                    assert.fail();
                    console.log(`Error: ${e}`);
                }
            })
            done();
        });

        it('Test: action = quit', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: "quit" }, player)));
            ws.on('message', (data) => {
                try {
                    if (JSON.parse(data).action === "quit") {
                        assert.ok(true);
                        console.log(`Action: Removed player`);
                    }
                } catch (e) {
                    assert.fail();
                    console.log(`Error: ${e}`);
                }
            })
            done();
        });

        it('Test: action = null', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: null }, player)));
            ws.on('message', (data) => {
                assert.equal(data, "Message Error.");
                console.log(`Response: ${data}`);
            })
            done();
        });

        it('Test: action = other', function (done) {
            ws.send(JSON.stringify(Object.assign({ action: "other" }, player)));
            ws.on('message', (data) => {
                assert.equal(data, "Action Undefined");
                console.log(`Response: ${data}`);
            })
            done();
        });

    });

});
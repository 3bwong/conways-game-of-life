'use strict';

const config = require('./config.json');
const _ = require('lodash');
const colors = config.colors;
const patterns = config.patterns;
const size = 20;
var rooms = [];

/**
 * Room Object:
 * id -> Timestamp get from open
 * players -> array of info for current room's players
 *      color -> color for the specific player
 *      ws -> websocket for the specific player
 * colors -> shuffled array of default colors
 * table -> current status for the cells
 */


/**
 * Action: New
 * Description: Adding New Player. Create new room if rooms.length==0 || all rooms colors.length==0
 * Request Body: { action: "new" }
 * Response:
 *      Request Player -> { action: "new", room ID, allocated color, default patterns }
 *      All Current Room Player -> sendUpdateRoom()
 */
function newPlayer(ws) {
    var room, color;
    if (rooms.length == 0 || !_.find(rooms, (val) => { return val.colors.length > 0 })) {
        room = Object.assign({}, createNewRoom());
        color = room.colors.shift();
        room.players.push({
            color: color,
            ws: ws
        })
        rooms.push(room)
    } else {
        room = _.find(rooms, (val) => { return val.colors.length > 0 });
        color = room.colors.shift();
        room.players.push({
            color: color,
            ws: ws
        })
    }
    ws.send(JSON.stringify({
        action: "new",
        roomId: room.id,
        roomIndex: _.findIndex(rooms, { id: room.id }),
        color: color,
        patterns: patterns
    }))
    sendUpdateRoom(_.findIndex(rooms, { id: room.id }));
}

/**
 * Function: WS send update action
 * Description: Send updated room info to specific room's players
 * Response:
 *      All Current Room Player -> { room (players: remove ws obj), action: "update" }
 */
function sendUpdateRoom(index) {
    var resRoom = Object.assign({}, rooms[index]);
    resRoom.players = _.keys(_.keyBy(resRoom.players, 'color'));
    rooms[index].players.forEach((player) => {
        player.ws.send(JSON.stringify({
            action: "update",
            room: resRoom
        }))
    })
}

/**
 * Function: Create New Room
 * Description: Create default room object.
 * Return: Room Object
 */
function createNewRoom() {
    var table = Array(size);
    for (var x = 0; x < size; x++) {
        table[x] = Array(size);
        for (var y = 0; y < size; y++) {
            table[x][y] = "#fff";
        }
    }
    return {
        id: Date.now().toString(),
        players: [],
        colors: _.shuffle(_.concat([], colors)),
        table: table
    }
}

/**
 * Action: Restore
 * Description: Restore the game from session storage (within 3 minutes) if connection lost or refresh page.
 *              Update player ws if room and player is found.
 *              Create new room if room or player not found.
 * Request Body: { action: "restore", roomId, allocated color }
 * Response:
 *      Room not found -> newPlayer()
 *      Room found -> sendUpdateRoom()
 */
function restoreGame(ws, data) {
    try{
        var roomIndex = _.findIndex(rooms, { id: data.roomId });
        if (roomIndex == -1) {
            newPlayer(ws);
        } else {
            var playerIndex = _.findIndex(rooms[roomIndex].players, { color: data.color });
            rooms[roomIndex].players[playerIndex].ws = ws;
            sendUpdateRoom(roomIndex);
        }
    }catch(e){
        console.log(e)
    }
}

/**
 * Action: Quit
 * Description: Quit game when quit game button is clicked. Remove player as soon as the button is clicked.
 * Request Body: {action: "quit", roomId, allocated color}
 * Response:
 *      Requested Player -> {action: "quit"}
 *      All Current Room Player -> sendUpdateRoom()
 */
function quitGame(ws, data) {
    try {
        var roomIndex = _.findIndex(rooms, { id: data.roomId });
        var playerIndex = _.findIndex(rooms[roomIndex].players, { color: data.color });
        _.pullAt(rooms[roomIndex].players, playerIndex); // Remove Player from Array
        rooms[roomIndex].colors.push(data.color); // Add Color back to Array
        if (rooms[roomIndex].colors.length == 10) { // Remove room if no player
            _.pullAt(rooms, roomIndex);
        } else {
            for (var x = 0; x < size; x++) { // Remove Color from table
                for (var y = 0; y < size; y++) {
                    (rooms[roomIndex].table[x][y] == data.color) ? rooms[roomIndex].table[x][y] = "#fff" : '';
                }
            }
            sendUpdateRoom(roomIndex);
        }
        ws.send(JSON.stringify({ action: "quit" }));
    } catch (e) {
        console.log(e);
        ws.close();
    }
}

/**
 * Function: Remove Player
 * Description: Remove player if they do not click quit game after 3 minutes.
 * Response:
 *      All Current Room Player -> sendUpdateRoom()
 */
function removePlayer(ws) {
    try {
        var roomIndex = _.findIndex(rooms, (room) => {
            return _.findIndex(room.players, { ws: ws }) > -1;
        });
        if (roomIndex > -1) {
            var playerIndex = _.findIndex(rooms[roomIndex].players, { ws: ws });
            var color = rooms[roomIndex].players[playerIndex].color;
            _.pullAt(rooms[roomIndex].players, playerIndex); // Remove Player from Array
            rooms[roomIndex].colors.push(color); // Add Color back to Array
            if (rooms[roomIndex].colors.length == 10) { // Remove room if no player
                _.pullAt(rooms, roomIndex);
            } else {
                for (var x = 0; x < size; x++) { // Remove Color from table
                    for (var y = 0; y < size; y++) {
                        (rooms[roomIndex].table[x][y] == color) ? rooms[roomIndex].table[x][y] = "#fff" : '';
                    }
                }
                sendUpdateRoom(roomIndex);
            }
        }
    } catch (e) {
        console.log(e);
    }
}

/**
 * Action: Update
 * Description: Update table whenever there is new life(s) added from player.
 * Request Body: {action: "update", roomId, color, type: "pattern"/"cell", pattern name / cell location}
 * Response:
 *      All Current Room Player -> sendUpdateRoom()
 */
function updateTable(data) {
    try {
        var roomIndex = _.findIndex(rooms, { id: data.roomId });
        if (data.type === "cell") {
            var location = data.location.split("-");
            if (rooms[roomIndex].table[Number(location[1])][Number(location[2])] === "#fff") {
                rooms[roomIndex].table[Number(location[1])][Number(location[2])] = data.color;
            }
            sendUpdateRoom(roomIndex);
        } else if (data.type === "pattern") {
            rooms[roomIndex].table = randomPattern(data.color, data.pattern, rooms[roomIndex].table);
            sendUpdateRoom(roomIndex);
        }
    } catch (e) {
        console.log(e);
    }
}

function randomPattern(color, pattern, table) {
    var newTable = Object.assign({}, table);
    var lifes = _.find(_.flatten(_.flatMap(patterns, (val) => { return val.lifes })), { name: pattern });
    var minX = _.orderBy(lifes.locations, ["x"], ["asc"])[0].x;
    var maxX = 19 - _.last(_.orderBy(lifes.locations, ["x"], ["asc"])).x;
    var minY = _.orderBy(lifes.locations, ["x"], ["asc"])[0].y;
    var maxY = 19 - _.last(_.orderBy(lifes.locations, ["y"], ["asc"])).y;
    var randX = _.random(minX, maxX);
    var randY = _.random(minY, maxY);
    var diffX = 0;
    var diffY = 0;
    var possessed = false;
    for (var i = 0; i < lifes.locations.length; i++) {
        if (i == 0) {
            if (newTable[randX][randY] === "#fff") {
                newTable[randX][randY] = color;
                diffX = randX - lifes.locations[i].x;
                diffY = randY - lifes.locations[i].y;
            } else {
                possessed = true;
                break;
            }
        } else {
            if (newTable[lifes.locations[i].x + diffX][lifes.locations[i].y + diffY] === "#fff") {
                newTable[lifes.locations[i].x + diffX][lifes.locations[i].y + diffY] = color;
            } else {
                possessed = true;
                break;
            }
        }
    }
    if (possessed) {
        randomPattern(color, pattern, table);
    } else {
        return newTable;
    }
}

/**
 * Function: Next Generation
 * Description: Evolve every three seconds. Apply algorithms to step to the next generation.
 * Response:
 *      All Current Room Player -> sendUpdateRoom()
 */
function nextGeneration() {
    if (rooms.length > 0) {
        rooms.forEach((room, index) => {
            var newTable = Array(size);
            var table = room.table;
            for (var x = 0; x < size; x++) {
                newTable[x] = Array(size);
                for (var y = 0; y < size; y++) {
                    var lifes = [];
                    if (x > 0 && y > 0 && table[x - 1][y - 1] && table[x - 1][y - 1] !== "#fff") {
                        lifes.push(table[x - 1][y - 1]);
                    }
                    if (y > 0 && table[x][y - 1] && table[x][y - 1] !== "#fff") {
                        lifes.push(table[x][y - 1]);
                    }
                    if (x < size - 1 && y > 0 && table[x + 1][y - 1] && table[x + 1][y - 1] !== "#fff") {
                        lifes.push(table[x + 1][y - 1]);
                    }
                    if (x < size - 1 && table[x + 1][y] && table[x + 1][y] !== "#fff") {
                        lifes.push(table[x + 1][y]);
                    }
                    if (x > 0 && table[x - 1][y] && table[x - 1][y] !== "#fff") {
                        lifes.push(table[x - 1][y]);
                    }
                    if (y < size - 1 && table[x][y + 1] && table[x][y + 1] !== "#fff") {
                        lifes.push(table[x][y + 1]);
                    }
                    if (x > 0 && y < size - 1 && table[x - 1][y + 1] && table[x - 1][y + 1] !== "#fff") {
                        lifes.push(table[x - 1][y + 1]);
                    }
                    if (x < size - 1 && y < size - 1 && table[x + 1][y + 1] && table[x + 1][y + 1] !== "#fff") {
                        lifes.push(table[x + 1][y + 1]);
                    }
                    if (lifes.length == 3 && table[x][y] === "#fff") { // average color
                        lifes = lifes.map((life) => {
                            return life.split(",");
                        })
                        var r = (Number(lifes[0][0]) + Number(lifes[1][0]) + Number(lifes[2][0])) / 3;
                        var g = (Number(lifes[0][1]) + Number(lifes[1][1]) + Number(lifes[2][1])) / 3;
                        var b = (Number(lifes[0][2]) + Number(lifes[1][2]) + Number(lifes[2][2])) / 3;
                        newTable[x][y] = r + "," + g + "," + b;
                    } else if (lifes.length > 3) {
                        newTable[x][y] = "#fff";
                    } else if (lifes.length < 2) {
                        newTable[x][y] = "#fff";
                    } else {
                        newTable[x][y] = table[x][y];
                    }
                }
            }
            room.table = newTable;
            sendUpdateRoom(index);
        })
    }
}

exports.newPlayer = newPlayer;
exports.updateTable = updateTable;
exports.restoreGame = restoreGame;
exports.removePlayer = removePlayer;
exports.quitGame = quitGame;
exports.nextGeneration = nextGeneration;